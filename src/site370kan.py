#!/usr/bin/env python2
# -*- coding:utf-8 -*-
from moviesite import Site, SiteException, Movie, MovieException, SearchException
from moviesite import Episode, ResourceGroup, SearchResult
from collections import OrderedDict
import re
from urllib import quote, unquote
from flask import url_for

title_pattern = re.compile(r'<title>\s*([^</ ]+)\s*[^<]*</title>', re.I|re.U)
js_pattern = re.compile(r'/playdata/(\d+)/(\d+)\.js')
video_pattern = re.compile(r"unescape\('([^']+)'\)")
page_pattern = re.compile(ur'页次:(\d+)/(\d+)页')

class MetaDataPicker:
    def pick(self, data):
        raise NotImplementedError()

class MetaRegExPicker(MetaDataPicker):
    group_idx = 0
    def pick(self, data):
        ans = list()
        for line in data:
            m = self.pattern.search(line)
            if m: ans.append(m.groups()[self.group_idx])
            # _logger.debug("%s %s", line, m)
        return ans

class TitlePicker(MetaRegExPicker):
    pattern = re.compile(r'<a\s+href="/kanview/kanindex\d+\.html"\s+title="([^"]+)"\s+target="_blank"', re.I)

class ImagePicker(MetaRegExPicker):
    pattern = re.compile(r'<img\s+src="(http://.*/uploadimg/.*\.(jpg|gif|png))"', re.I)

class IdPicker(MetaRegExPicker):
    pattern = re.compile(r'<a\s+href="/kanview/kanindex(\d+)\.html"\s+title="[^"]+"\s+target="_blank"', re.I)

class List370Kan(Site):
    name          = '370kan'
    title_pattern = title_pattern
    js_pattern    = js_pattern
    video_pattern = video_pattern
    page_pattern  = page_pattern
    urlbase       = {'js':'http://www.370kan.com/playdata/{0}/{1}.js',
               'movie':'http://www.370kan.com/player/index{0}.html',
               'search':'http://www.370kan.com/search.asp'
              }
    headers       = {
                'User-Agent':r'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.19 Safari/537.31', 
                'Referer':'http://www.370kan.com/',
                'Origin':'http://www.370kan.com/',
                'Content-Type':'application/x-www-form-urlencoded'
                }
    
    def search(self, keywords, page):
        params = {
                'searchword':keywords.encode('gbk'),
                'submit':'',
                'page':page
                }
        page = self._open_url_helper(self.urlbase['search'], method='post', headers=self.headers,
                                        data=params, exception=SearchException)
        page.encoding = 'gb2312'
        found = page.text.split('\n')
        titles = TitlePicker().pick(found)
        images = ImagePicker().pick(found)
        ids = IdPicker().pick(found)
        if len(set([len(titles), len(images), len(ids)])) > 1:
            self._logger("Different length of tuples: %d, %d, %d", len(titles), len(images), len(ids))
            self._logger("Different length of tuples: %s, %s, %s", titles, images, ids)
            return None
        movies = []
        for title, thumbnail, id_ in zip(titles, images, ids):
            movies.append(Movie(id_, title, thumbnail, None, None))

        pages = self.page_pattern.search(page.text)
        if pages:
            generate_link = \
                    lambda i: (i, '?page={}&keyword={}'.format(i, self._quote_kw(keywords)))
            curpage, totalpages = list(map(int, pages.groups()[:2]))
            prevpages, nextpages = self._get_page_links(curpage, totalpages, generate_link)
        else:
            curpage = 0
            totalpages = 0
            prevpages = None
            nextpages = None

        self.logger(prevpages)
        self.logger(nextpages)

        return SearchResult(keywords, movies, curpage, totalpages, prevpages, nextpages)

    def _quote_kw(self, kw):
        return quote(kw.encode('utf8'))

    def _get_page_links(self, curpage, totalpages, linkgenerator, firstpage=1, linknum=8):
        left_room = curpage - firstpage
        right_room = totalpages - curpage
        if left_room + right_room < linknum - 1:
            # too narrow, just return the whole space
            startidx = firstpage
            endidx = totalpages
        elif left_room < (linknum - 1) / 2:
            # current page near to first page, shift window to right
            startidx = firstpage
            endidx = curpage + ( linknum - 1 - left_room )
        else:
            # current page near to max page, shift window to left
            endidx = totalpages
            startidx = curpage - ( linknum - 1 - right_room )

        prevs = [linkgenerator(i) for i in range(startidx, curpage)]
        nexts = [linkgenerator(i) for i in range(curpage+1, endidx+1)]
        return (prevs, nexts)



    def get(self, movid):
        moviepage = self._open_url_helper(self.urlbase['movie'].format(movid),
                                            headers=self.headers, exception=MovieException)
        # 370kan use meta tag to indicate codec, Requests can't find it
        # TODO: use regex to locate the encoding
        moviepage.encoding='gb2312'
        title_match = self.title_pattern.search(moviepage.text)

        title = title_match.groups()[0] if title_match else ('movie_%s' %(movid,))
        self.logger(u'get a movie {}'.format(title))

        js_ids = self.js_pattern.search(moviepage.text) 
        if not js_ids:
            raise MovieException(self.name, 'no js file in player page of %s'%(title,))
        self.logger(u'get js ids {},{}'.format(*js_ids.groups()))

        jspage = self._open_url_helper(self.urlbase['js'].format(*js_ids.groups()),
                                        headers=self.headers, exception=MovieException)
        resources = self._parse_playdata(jspage.text, title)
        return Movie(movid, title, '', resources, None)

    def _unescape(self, content):
        unicodify = lambda s:unichr(int(s[0:4], 16))+unicode(s[4:])
        return unicode(content.split('%u',1)[0]) + u''.join([unicodify(s) for s in content.split('%u')[1:]])

    def _parse_playdata(self, content, moviename):
        content = self._unescape(unquote(content))
        videos = self.video_pattern.search(content)
        deflated = videos.groups()[0]
        from_tree = OrderedDict()
        strip_server_idx = lambda name: name.split('_')[0]
        bypass_formatter = lambda n, u, e: (n, u, e, False)
        additional_formatter = {
                'youku': lambda n, u, e: (n, \
                                        url_for('player_youku', \
                                                t='%s - %s'%(moviename, n), \
                                                vid=(u[u.index('id_')+3:u.index('.html')] if 'id_' in u and '.html' in u else u),\
                                                _external=True\
                                               ),\
                                        #u if 'youku' in u else 'http://v.youku.com/v_show/id_{}.html'.format(u), \
                                        e, True),
                'qiyi': lambda n, u, e: (n, \
                                    u if 'iqiyi' in u else url_for('player_iqiyi', vid=u, t='%s - %s'%(moviename, n), _external=True),
                                    e, True)
                }

        for idx, source in enumerate(deflated.split('$$$')):
            from_tree["%s_server%02d"%(source.split('$$')[0], idx)] = source.split('$$')[1].split('#') 

        # from_tree = { \
        #             "%s_server%02d"%(source.split('$$')[0], idx): \
        #                 source.split('$$')[1].split('#') for idx, source in enumerate(deflated.split('$$$'))\
        #             }
        ans = []
        for k, v in from_tree.items():
            rg = ResourceGroup(strip_server_idx(k), [], None)
            ans.append(rg)
            for episode in v:
                name, url, ext = tuple(map(lambda x:x.strip().strip('\n'),episode.split('$')[0:3]))
                name, url, ext, add_link = \
                        additional_formatter.get(ext, bypass_formatter)(name, url, ext)
                r = Episode(name, name, url, ext, add_link)
                rg.episodes.append(r)

        return ans

if __name__ =='__main__':
    s = List370Kan()
    print(s.get('10369'))
