#!/usr/bin/env python2
# -*- coding:utf-8 -*-
from collections import namedtuple
import requests

Movie = namedtuple('Movie', 'id, title, thumbnail, resource_groups, ext')
ResourceGroup = namedtuple('ResourceGroup', 'player, episodes, ext')
Episode = namedtuple('Episode', 'name, desc, url, ext, add_link')
SearchResult = namedtuple('SearchResult', 'keyword, movies, curpage, maxpage, prevpages, nextpages')

class SiteException(Exception):
    def __init__(self, sitename, message, errcode=500, **kwargs):
        Exception.__init__(self)
        self.message = message
        self.errcode = errcode
        self.other = kwargs
        self.sitename = sitename

    def to_dict(self):
        rv = dict()
        rv['message'] = self.message
        rv['sitename'] = self.sitename
        if self.errcode != None:
            rv['code'] = self.errcode
        if self.other:
            rv.update(self.other)
        return rv

class SearchException(SiteException):
    pass

class MovieException(SiteException):
    pass

def dummy_logger(*args, **kwargs):
    pass

class Site:
    """Site defines interfaces should be implemented to support movie listing in a
 service website"""

    name = "unknown"
    """Name of this site"""

    logger = dummy_logger

    def search(self, keywords, *args, **kwargs):
        """Search for keyword in a site and returns list of search results.
           Return None if nothing found.
           Raise SearchException when error encountered.
        """
        return None
    
    def get(self, movieid):
        """Return detail information of the movie specified by its ID.
           Or raise MovieException.
        """
        raise NotImplementedError();

    def _open_url_helper(self, url, data=None, headers=None, exception=SiteException, method='get'):
        try:
            url_response = getattr(requests, method.lower())(url, data=data, headers=headers)
        except requests.exceptions.RequestException as err:
            raise exception(self.name, str(err), errcode=err.errno)
        except Exception as err:
            raise exception(self.name, str(err), errcode=500)
        if url_response:
            return url_response
        else:
            raise exception(self.name, "empty response received from %s"%(url,), errcode=500)

