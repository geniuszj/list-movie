#!/usr/bin/env python2
# -*- coding:utf-8 -*-

from flask import Flask, render_template, request, redirect
from site370kan import List370Kan

app = Flask('list370kan')
app.config['SERVER_NAME'] = 'list370kan.genzj.info'

@app.route('/movie/370kan/<movid>')
def get_movie(movid):
    s = List370Kan()
    s.logger = app.logger.info
    return render_template('movie.html', movie=s.get(movid))

@app.route('/search', methods=['POST', 'GET'])
def search():
    keyword = request.form['keyword'] if request.method=='POST' else request.args.get('keyword','')
    page = request.form['page'] if request.method=='POST' else request.args.get('page','1')
    s = List370Kan()
    s.logger = app.logger.info
    return render_template('search.html', result=s.search(keyword, page))

@app.route('/')
@app.route('/index.html')
@app.route('/index.htm')
def index():
    return redirect('/static/index.html')

@app.route('/player/iqiyi')
def player_iqiyi():
    vid = request.args.get('vid', '')
    title = request.args.get('t', 'iQiyi Player')
    if not vid:
        redirect('/static/index.html')
    else:
        player = { 'vid': vid, 'title': title }
        return render_template('qiyiplayer.html', player=player)

@app.route('/player/youku')
def player_youku():
    vid = request.args.get('vid', '')
    title = request.args.get('t', 'YouKu Player')
    if not vid:
        redirect('/static/index.html')
    else:
        player = { 'vid': vid, 'title': title }
        return render_template('youkuplayer.html', player=player)

if __name__=='__main__':
    app.debug = True
    app.run(port=8000)
